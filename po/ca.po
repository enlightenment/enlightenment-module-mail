msgid ""
msgstr ""
"Project-Id-Version: Marc Furtià  i Puig\\\n"
"Report-Msgid-Bugs-To: $MSGID_BUGS_ADDRESS\n"
"POT-Creation-Date: 2013-08-24 11:51+0200\n"
"PO-Revision-Date: 2014-05-27 10:02+0000\n"
"Last-Translator: joancoll <Unknown>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2014-05-28 05:31+0000\n"
"X-Generator: Launchpad (build 17017)\n"
"Language: ca\n"
"X-Poedit-Bookmarks: -1,-1,1139,-1,-1,-1,-1,-1,-1,-1\n"

#: src/e_mod_config_box.c:68
msgid "Mailbox Configuration"
msgstr "Configuració bústia d'entrada"

#: src/e_mod_config_box.c:160 src/e_mod_config.c:94
msgid "General Settings"
msgstr "Preferències Generals"

#: src/e_mod_config_box.c:162
msgid "Start Program When New Mail Arrives"
msgstr "Inicia programa quan arribi nou correu"

#: src/e_mod_config_box.c:167
msgid "Program:"
msgstr "Programa:"

#: src/e_mod_config_box.c:179
msgid "Mailbox Type"
msgstr "Tipus bústia d'entrada"

#: src/e_mod_config_box.c:181
msgid "Pop3"
msgstr "Pop3"

#: src/e_mod_config_box.c:184
msgid "Imap"
msgstr "Imap"

#: src/e_mod_config_box.c:187
msgid "Maildir"
msgstr "Directori de correu"

#: src/e_mod_config_box.c:190
msgid "Mbox"
msgstr "Mbox"

#: src/e_mod_config_box.c:193
msgid "Monitor Mbox file permanently"
msgstr "Monitoritza fitxer Mbox contínuament"

#: src/e_mod_config_box.c:210
msgid "Port Settings"
msgstr "Configuració del port"

#: src/e_mod_config_box.c:212
msgid "Use SSL:"
msgstr "Utilitza SSL:"

#: src/e_mod_config_box.c:219
msgid "v2"
msgstr "v2"

#: src/e_mod_config_box.c:222
msgid "v3"
msgstr "v3"

#: src/e_mod_config_box.c:226
msgid "Port:"
msgstr "Port:"

#: src/e_mod_config_box.c:232
msgid "Local:"
msgstr "Local:"

#: src/e_mod_config_box.c:238
msgid "Mailbox Settings"
msgstr "Configuració bústia de correu"

#: src/e_mod_config_box.c:239
msgid "Name:"
msgstr "Nom:"

#: src/e_mod_config_box.c:244
msgid "Mail Host:"
msgstr "Host de correu:"

#: src/e_mod_config_box.c:249
msgid "Username:"
msgstr "Nom d'usuari:"

#: src/e_mod_config_box.c:254
msgid "Password:"
msgstr "Contrasenya:"

#: src/e_mod_config_box.c:260
msgid "New Mail Path:"
msgstr "Trajectòria correu nova:"

#: src/e_mod_config_box.c:272
msgid "Current Mail Path:"
msgstr "Trajectòria correu actual:"

#: src/e_mod_config_box.c:430
msgid "Inbox"
msgstr "Bústia d'entrada"

#: src/e_mod_config.c:53
msgid "Mail Settings"
msgstr "Configuració de correu"

#: src/e_mod_config.c:96
msgid "Always Show Labels"
msgstr "Mostra etiquetes sempre"

#: src/e_mod_config.c:99
msgid "Show Mailbox Popup"
msgstr "Mostra bústia de correu en finestra emergent"

#: src/e_mod_config.c:102
msgid "Show All Boxes In Popup"
msgstr "Mostra totes les bústies en finestra emergent"

#: src/e_mod_config.c:104
msgid "Check Interval"
msgstr "Interval de comprovació"

#: src/e_mod_config.c:107
#, c-format
msgid "%2.0f minutes"
msgstr "%2.0f minuts"

#: src/e_mod_config.c:112
msgid "Mail Boxes"
msgstr "Bústies de correu"

#: src/e_mod_config.c:122
msgid "Add"
msgstr "Afegir"

#: src/e_mod_config.c:126
msgid "Delete"
msgstr "Borra"

#: src/e_mod_config.c:132
msgid "Configure..."
msgstr "Configura..."

#: src/e_mod_main.c:186
msgid "Mail"
msgstr "Correu"

#: src/e_mod_main.c:230
msgid "Settings"
msgstr "Preferències"
